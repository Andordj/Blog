---
title: Week acht, EXPO tijd.
subtitle: Laatste post dit kwartaal
date: 2017-10-26
---
*Week 43, 23-10-2017 t/m 27-10-2017*  
**Maandag 23-10**  
Deze design challenge les zijn mijn projectgroep en ik voornamelijk bezig geweest met de voorbereiding op de EXPO die plaats zal vinden op woensdag 25-10. Ik heb zelf de pitch voorbereid en we hebben gewerkt aan de opmaak van de tafel.  
**Dinsdag 24-10**  
Het hoorcollege ging niet door.
Vanwege ziekte ook de photoshop cursus niet kunnen bijwonen.  
**Woensdag 25-10**  
EXPO 1e jaars HRO mede studenten. Deze dag stond in het teken van alle projecten van het afgelopen kwartaal. Na afloop van de EXPO een studiecoaching les gehad, dit ging over een tweede peerfeedback ronde, en de laatste ountjes op de i voor het leerdossier.  
**Donderdag 26-10**  
Afronden van mijn leerdossier en een laatste keer mijn blog updaten.  
**Vrijdag 27-10**  
Inleveren van het leerdossier.